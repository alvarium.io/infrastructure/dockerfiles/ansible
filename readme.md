Ansible Container
=================

[![pipeline status][pipeline svg]][pipeline]
[![Docker Pulls][pulls svg]][docker hub]

This is a container based on ubuntu 18.04 just with ansible installed. You can use it to not install ansible-playbook on your machine, and use docker instead:

~~~bash
docker run --rm alvarium/ansible ansible-playbook --version
docker run --rm alvarium/ansible ansible-playbook -i inventory.yml my-playbook-file.yml
~~~

License
-------

This project has been created by Òscar Casajuana <oscar@alvarium.io> and [is licensed under the GNU-GPL-3.0 license][license].


[pipeline svg]: https://gitlab.com/alvarium.io/infrastructure/dockerfiles/ansible/badges/master/pipeline.svg
[pulls svg]: https://img.shields.io/docker/pulls/alvarium/ansible.svg

[pipeline]: https://gitlab.com/alvarium.io/infrastructure/dockerfiles/ansible/commits/master
[docker hub]: https://hub.docker.com/r/alvarium/ansible/

[license]: ./LICENSE.md